$(document).ready(function(){
    var isInViewport = function (elem) {
        var distance = elem.getBoundingClientRect();
        return (
            distance.top >= -100 &&
            distance.left >= 0 &&
            distance.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
            distance.right <= (window.innerWidth || document.documentElement.clientWidth)
        );
    };

    function isElementInViewport(elem) {
        var $elem = $(elem);
    
        // Get the scroll position of the page.
        var scrollElem = ((navigator.userAgent.toLowerCase().indexOf('webkit') != -1) ? 'body' : 'html');
        var viewportTop = $(scrollElem).scrollTop();
        var viewportBottom = viewportTop + $(window).height();
    
        // Get the position of the element on the page.
        var elemTop = Math.round( $elem.offset().top );
        var elemBottom = elemTop + $elem.height();
    
        return ((elemTop < viewportBottom) && (elemBottom > viewportTop));
    }

    var flexcontainer = document.querySelector('.letterhead');
    var flexcontainer2 = document.querySelector('#locationdata');
    
    TweenMax.to('#sub-title',1.5,{opacity:1, x:0})
    TweenMax.to('.links-training',1.5,{opacity:1, x:0})
    // const controller = new ScrollMagic.Controller();
    //     $('.card-week').each(function() {
    //         var title = $(this).find(".title");
    //         var option = $(this).find(".option")
    //         var tl = new TimelineMax();
    //         tl.from(title, .5, {opacity:0, scale:0});
    //         tl.from(option, .6, {opacity:0, scale:0});
    //         const scene = new ScrollMagic.Scene({
    //             triggerElement: this
    //         }).setTween(tl).addTo(controller); 
    //     });
        window.addEventListener('scroll', function (event) {
            if (isInViewport(flexcontainer)) {
                TweenMax.to('.imagetwo',1,{opacity:1, x:0})
                TweenMax.to('.imageone',1,{opacity:1, x:-100})
                TweenMax.to('.rightletter',1,{opacity:1, x:0})
            }
            if (isInViewport(flexcontainer2)) {
                // TweenMax.to('.statsrightdata',1,{opacity:1, x:0})
                TweenMax.to('.leftmapimage',1,{opacity:1, x:0})
            }
        }, false);
}, false);


    /* if (isInViewport(this)) {
        alert(this);
        alert('in view');
        TweenMax.to(this,1.5,{opacity:1, y:0})
    } */

    /* $('#gen').click(function(){
        TweenMax.to('#general-container',1.5,{opacity:1, y:0, display:"flex"})
    })
    $('#tech').click(function(){
        TweenMax.to('#tech-container',1.5,{opacity:1, y:0, display:"flex"})
    })
    $('#sports').click(function(){
        TweenMax.to('#sports-container',1.5,{opacity:1, y:0, display:"flex"})
    })
    $('#cult').click(function(){
        TweenMax.to('#cult-container',1.5,{opacity:1, y:0, display:"flex"})
    })
    $('#support').click(function(){
        TweenMax.to('#support-container',1.5,{opacity:1, y:0, display:"flex"})
    }) */