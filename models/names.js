const Sequelize = require('sequelize');
const sequelize = require('../helpers/database');
const { NULL } = require('node-sass');

const Name = sequelize.define('name', {
    id: {
        type:Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    name:{
        type:Sequelize.STRING,
        allowNull:false
    },
    email:{
        type:Sequelize.STRING,
        allowNull:true
    },
    secret:{
        type:Sequelize.STRING,
        allowNull:true
    }
},
{
    timestamps: false,
})

module.exports = Name;