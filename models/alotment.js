const Sequelize = require('sequelize');
const sequelize = require('../helpers/database');
const { NULL } = require('node-sass');

const Alotment = sequelize.define('alotment', {
    id: {
        type:Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    name:{
        type:Sequelize.STRING,
        allowNull:false
    },
    email:{
        type:Sequelize.STRING,
        allowNull:false
    },
    address:{
        type:Sequelize.STRING,
        allowNull:false
    },
    data:{
        type:Sequelize.TEXT,
        allowNull:false
    },
    contact:{
        type:Sequelize.STRING,
        allowNull:false
    },
    alotmentbool:{
        type:Sequelize.BOOLEAN,
        allowNull: false,
        default: 0,
    }
},
{
    timestamps: false,
})

module.exports = Alotment;