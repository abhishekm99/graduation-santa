$(document).ready(function(){
    $('#description').click(function(){
        $('.achievements').addClass('none');
        $('.contact').addClass('none');
        $('.description').removeClass('none')
        $('.video').addClass('none')
    });
    $('#achievements').click(function(){
        $('.description').addClass('none');
        $('.contact').addClass('none');
        $('.achievements').removeClass('none')
        $('.video').addClass('none')
    });
    $('#contact').click(function(){
        $('.description').addClass('none');
        $('.achievements').addClass('none');
        $('.contact').removeClass('none')
        $('.video').addClass('none')
    })

    $('.clubvidid').click(function(){
        $('.description').addClass('none');
        $('.achievements').addClass('none');
        $('.contact').addClass('none')
        $.get(`/api/club/getvideo?clubid=${$(this).attr('id')}`, (result) => {
            $('#clubvid').empty();
            if(result.success){
                $('#clubvid').append(`
                <iframe src="${result.link}/preview" width="100%" height="300px"></iframe>
                `)
                $('.video').removeClass('none')
            }else{
                $('#clubvid').append(`
                <p>No Video Uploaded</p>
                `)
                $('.video').removeClass('none')
            }
        })
    })
})