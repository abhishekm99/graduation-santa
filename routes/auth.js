var express = require('express');
const { check, validationResult } = require('express-validator');
var router = express.Router();
const multer = require("multer")

var loginController = require('../controllers/admin/login');
const clubController = require('../controllers/clubs');
const eventController = require('../controllers/events');
const isAuth = require('../middlewares/is-auth');

const fileFilter = (req,file,cb) => {
    if(file.mimetype.startsWith("image")){
        cb(null, true);
    } else {
        cb(null, false);
    }
}
var storage = multer.diskStorage({ 
    destination: (req, file, cb) => {
        cb(null, __dirname + '/../public/uploads/')
      },
    filename: function (req, file, cb) { 
      console.log(file)
      let filename = file.originalname.split('.')[0]
      filename = filename.split(' ')[0]
      cb(null, filename + "-" + Date.now()+".jpg") 
    } 
  }) 

var upload = multer({ storage: storage,fileFilter: fileFilter})
// var uploadedit = multer({ storage: storage })

router.get('/', loginController.getLogin);
router.get('/login', loginController.getLogin);
router.post('/login', loginController.postLogin);
router.get('/dashboard', isAuth, loginController.getDashboard);
// router.get('/register',  loginController.getRegister);
// router.post('/register',  loginController.postRegister);
router.get('/fpwd', loginController.getFPwd);
router.post('/fpwd', loginController.postReset);
router.get('/resetp/:token', loginController.getNewPassword);
router.post('/rpwd', loginController.postNewPassword);
router.post('/events/start/:eventid',isAuth, loginController.startEvent);
router.post('/events/stop/:eventid',isAuth, loginController.stopEvent);
router.post('/events/delete/:eventid',isAuth, loginController.deleteEvent);
router.get('/events/edit/:eventid',isAuth, loginController.editEvent);
router.post('/events/edit/:eventid',isAuth, upload.single('imageUrl'), loginController.postEditEvent);
router.get('/events/create',isAuth, loginController.createNewEvent)
router.post('/events/create',isAuth, upload.single('imageUrl'),
[
  check('eventname').isLength({ min: 3 }).withMessage('Please Enter a proper name'),
  check('desc').isLength({ min: 10 }).withMessage('Please Enter a longer description'),
  check('contactname').isLength({ min: 3 }).withMessage('Please Enter proper contact name'),
  check('contact').isLength({min:9}).withMessage('Please enter a proper mobile number'),
  check('startdate').isDate().withMessage('Please enter a proper date for start date'),
], loginController.postCreateNewEvent)
router.post('/events/approve/:eventid',isAuth, loginController.approveEvent)
router.post('/events/disapprove/:eventid',isAuth, loginController.disapproveEvent)
router.get('/events/participants/:eventid', isAuth, loginController.getParticipantList);
router.get('/events/participants', isAuth, loginController.getAllParticipantList);
router.get('/clubs/notifications', loginController.getNotification);
router.post('/clubs/notifications/markasread', eventController.markNotification);
router.get('/events/sendblast', isAuth, loginController.blastEmails);
router.get('/events/sendblastemails', isAuth, loginController.getBlastEmails);
router.get('/events/blasttemplates', isAuth, loginController.getBlastTemplate);
router.get('/events/participants/sendemail/:eventid', isAuth, loginController.getSendEmail);
router.get('/faq/create', isAuth, clubController.getcreateFAQ);
router.post('/faq/create', isAuth, clubController.postcreateFAQ);
router.get('/faq/editfaq/:faqid', isAuth, clubController.geteditFAQ);
router.post('/faq/editfaq/:faqid', isAuth, clubController.posteditFAQ);
router.post('/faq/deletefaq/:faqid', isAuth, clubController.deleteFAQ);
router.get('/faq/questions/all/:clubid', isAuth, clubController.getQuestions);
router.get('/faq/answerquestion/:faqid', isAuth, clubController.getanswerQuestion);
router.post('/faq/answerquestion/:faqid', isAuth, clubController.postanswerQuestion);

router.post('/faq/markfaq/:faqid', isAuth, clubController.markAsFAQ);
router.post('/faq/unmarkfaq/:faqid', isAuth, clubController.unMarkAsFAQ);

router.get('/populateevents', isAuth, eventController.populateEvents);
router.get('/councilpopulateevents', isAuth, eventController.councilPopulateEvents);

router.post('/logout', isAuth, loginController.postLogout);
module.exports = router;