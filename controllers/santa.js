
const mysql = require('mysql');
const Names = require('../models/names');
const Alotment = require('../models/alotment')
const Finallist = require('../models/finallist')
const dotenv = require('dotenv');
const sequelize = require('../helpers/database');
const Sequelize = require('sequelize');
const { search } = require('../app');
const { query } = require('express');
const Op = Sequelize.Op;
const uuidv4 = require('uuid/v4');
const nodemailer = require('nodemailer')
const { check, validationResult } = require('express-validator');
const awsconfig = require('../config/config');

const aws = require('aws-sdk');

aws.config.update(awsconfig);

let transporter = nodemailer.createTransport({
    SES: new aws.SES({
        apiVersion: '2010-12-01'
    })
});

dotenv.config();

exports.getRegisterNames = (req,res,next) => {
    Names.findAndCountAll({where:{secret:null}}).then(names => {
        console.log(names)
        var promises = names.rows.map(async name => {
            var nameobj = name.name
            return nameobj
        })
        Promise.all(promises).then(function(results) {
            res.json(results);
        })
        // res.json(names.rows)
        // allnames = []
        // names.map(name => {
        //     allnames.push(name.name)
        // }).then(result => {
        //     res.json(allnames)
        // })
    }).catch(err => console.log(err))
}


exports.getNames = (req,res,next) => {
    Names.findAndCountAll().then(names => {
        var promises = names.rows.map(async name => {
            var nameobj = name.name
            return nameobj
        })
        Promise.all(promises).then(function(results) {
            res.json(results);
        })
        // res.json(names.rows)
        // allnames = []
        // names.map(name => {
        //     allnames.push(name.name)
        // }).then(result => {
        //     res.json(allnames)
        // })
    })
}

function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * 
 charactersLength));
   }
   return result;
}

exports.registerDetails = (req,res,next) => {
    var name = req.body.name;
    var email = req.body.email;
    var fulladdress = req.body.flat.concat(", ").concat(req.body.locat).concat(", ").concat(req.body.state).concat(" - ").concat(req.body.pin);
    var data = req.body.interests;
    var secret = makeid(8);
    Alotment.create({
        name: name,
        email: email,
        address: fulladdress,
        data: data,
        contact: req.body.contact,
        alotmentbool : 0,
    }).then(result => {
        Names.findOne({where:{name: name}}).then(result => {
            result.email = email;
            result.secret = secret;
            result.save();
        })
        transporter.sendMail({
            to:email,
            from:'Graduation Santa <noreply.events@mitsc.in>',
            subject: `Your Registration is complete`,
            html:`
            <div class="container">
                <div class="top" style="height:100px; background-color: #f2f2f2;">
                    <center>
                        <h2>Graduation Santa</h2>
                    </center>
                </div>
                <div class="midd" style="height:300px">
                    <center>
                        <h3 style="font-style: italic;color: #919191;">Hello from Graduation Santa!</h3>
                        <h5 style="font-style: italic;color: #919191;">Dear ${name}, </h5>
                        <h1 style="font-weight: bold;">Please find your allotment details as below</h1>
                        <div class="resulttable" style="text-align: center; width: 100%;">
                            <table style="width:90%">
                                <tr>
                                <th style="border: 1px solid #ddd; padding: 8px;">Name</th>
                                <td style="border: 1px solid #ddd; padding: 8px;">${name}</td>
                                </tr>
                                <tr>
                                <th style="border: 1px solid #ddd; padding: 8px;">Address</th>
                                <td style="border: 1px solid #ddd; padding: 8px;">${fulladdress}</td>
                                </tr>
                                <tr>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Info about you</th>
                                    <td style="border: 1px solid #ddd; padding: 8px;">${data}</td>
                                </tr>
                                <tr>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Secret Key</th>
                                    <td style="border: 1px solid #ddd; padding: 8px;">${secret}</td>
                                </tr>
                            </table>
                        </div> 
                    </center>
                </div>
            </div>
            `,
        });
        res.json('Thank you For Registering, please check your email for special code. Keep it handy as you will need it during alotment')
    })
}

exports.makeAlotment = (req,res,next) => {
    var name = req.body.name;
    var email = req.body.email;
    Alotment.findOne({ order: Sequelize.literal('rand()'), where:{name: { [Op.not]: name} ,alotmentbool: false}}).then(result => {
        result.update({alotmentbool : 1})
        Finallist.create({
            primaryname: name,
            secondaryname: result.dataValues.name
        }).then(result => {
            Names.destroy({where: {name: name}})
        })
        transporter.sendMail({
            to:email,
            from:'Graduation Santa <noreply.events@mitsc.in>',
            subject: `Your allotment as a graduation santa`,
            html:`
            <div class="container">
                <div class="top" style="height:100px; background-color: #f2f2f2;">
                    <center>
                        <h2>Graduation Santa</h2>
                    </center>
                </div>
                <div class="midd" style="height:300px">
                    <center>
                        <h3 style="font-style: italic;color: #919191;">Hello from Graduation Snta!</h3>
                        <h5 style="font-style: italic;color: #919191;">Dear ${name}, </h5>
                        <h1 style="font-weight: bold;">Please find your allotment details as below</h1>
                        <div class="resulttable" style="text-align: center; width: 100%;">
                            <table style="width:90%">
                                <tr>
                                <th style="border: 1px solid #ddd; padding: 8px;">Name</th>
                                <td style="border: 1px solid #ddd; padding: 8px;">${result.dataValues.name}</td>
                                </tr>
                                <tr>
                                <th style="border: 1px solid #ddd; padding: 8px;">Address</th>
                                <td style="border: 1px solid #ddd; padding: 8px;">${result.dataValues.address}</td>
                                </tr>
                                <tr>
                                <th style="border: 1px solid #ddd; padding: 8px;">Contact Number</th>
                                <td style="border: 1px solid #ddd; padding: 8px;">${result.dataValues.contact}</td>
                                </tr>
                                <tr>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Info about them</th>
                                    <td style="border: 1px solid #ddd; padding: 8px;">${result.dataValues.data}</td>
                                </tr>
                            </table>
                        </div> 
                    </center>
                </div>
            </div>
            `,
        }).then(result => {
            res.json('Please check your email for the details')
        });
    })
}