const nodemailer = require('nodemailer')
const awsconfig = require('../config/config');

const aws = require('aws-sdk');

aws.config.update(awsconfig);

let transporter = nodemailer.createTransport({
    SES: new aws.SES({
        apiVersion: '2010-12-01'
    })
});

exports.sendEmail = (to,subject,body) => {
    transporter.sendMail({
        to:to,
        from:"Events Portal <noreply.events@mitsc.in>",
        subject: subject,
        html:`
        <div class="container">
            <div class="top" style="height:100px; background-color: #f2f2f2;">
                <center>
                    <h2>Events Portal</h2>
                </center>
            </div>
            <div class="midd" style="min-height:300px; height:100%; padding: 20px;">
                ${body}
            </div>
        </div>
        `,
    }).then(result => {
        console.log(result);
    });
}
