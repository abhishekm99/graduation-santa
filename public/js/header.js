$(document).ready(function(){
    $(".menu-icon").on("click", function(){
        $("nav ul").toggleClass("showing");
    });

    $('#gotoabout').on("click", function(){
        window.location = "/#following_section"
    })

    $('#faq-button').on("click", function(){
        window.location.href='./race-info#faq-section';
    })

    $('#sponsor-button').on("click", function(){
        window.location.href='./#sponsors';
    })

    $('#vso-button').on("click", function(){
        window.location.href='./organiser#vso-section';
    })
});

$(window).on("scroll", function(){
    if($(window).scrollTop()) {
        $('nav').addClass('black');
        $('.header_logo').removeClass('show');
        $('.logo').addClass('show');
        $(".dropdown-content").addClass("black");
        if ($(window).width() < 700){
            $('.logo').addClass('hide');
        }
        $('.move').attr('x', '25%');
    }
    else
    {
        $('nav').removeClass('black');
        $('.header_logo').addClass('show');
        $('.logo').removeClass('show');
        if ($(window).width() < 700){
            $('.logo').removeClass('hide');
        }
        $('.move').attr('x', '10');
    }
})

