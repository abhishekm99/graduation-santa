$(document).ready(function() {
    $('#searchbutton').click(function(){
        let name = $('#ename').val();
        let date = $('#edate').val();
        if(!date && !name){
            window.location.href = `/clubs/events`;
        }else{
            window.location.href = `/clubs/events/searchq?searchn=${name}&searchd=${date}`;
        }
    })

    $.getJSON('/clubs/events/list', function(data) {
        data.names.forEach(element => {
            $('#clubname').append(new Option(element, element))
        });
        data.tags.forEach(element => {
            $('#eventtype').append(new Option(element, element))
        });
    });

    $('#eventtype').on('change', function() {
        window.location.href = `/clubs/events/sortq?sortt=${this.value}`;
    });
    $('#clubname').on('change', function() {
        window.location.href = `/clubs/events/sortq?sortn=${this.value}`;
    });

    $('.eventcard').click(function(){
        let id = $(this).attr("id");
        window.location.href = `/clubs/events/${id}`;
    })

    $('.button').click(function(){
        let id = $(this).attr("id");
        window.location.href = `/clubs/${id}`;
    })

    $('#mc-embedded-subscribe').click(function(event){
        event.preventDefault();
        $.post('/events/promotions/subscribe', {name: document.getElementById('mce-name').value, email: document.getElementById('mce-email').value}, (result => {
            console.log(result.msg)
        }))
        document.getElementById('thanks').style.display = 'block';
        document.getElementById('nothanks').style.display = 'none';
    })
})