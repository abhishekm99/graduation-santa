var express = require('express');
const { check, validationResult } = require('express-validator');
var router = express.Router();
const santaController = require('../controllers/santa');
// const eventsController = require('../controllers/events');
const http = require('http')
const fs = require('fs')

const captcha = require('../helpers/recaptchaRedirect');


// const mysql = require('mysql');

const dotenv = require('dotenv');

dotenv.config();

// function handleDisconnect() {

//   const db = mysql.createConnection ({
//     host: 'localhost',
//     user: 'Admin_default',
//     password: `${process.env.MYSQL_PASS}`,
//     database: 'clubs',
//   });
//   try{
//   db.connect((err) => {
//     if (err) {
//         throw err;
//     }
//     console.log('Connected to database');
//   });
//   global.db = db;
//   }
//   catch(err) {
//     console.log(err);
//   }
  
  
//   db.on('error', function(err) {
//     console.log('db error', err);
//     if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
//       handleDisconnect();                         // lost due to either server restart, or a
//     } else {                                      // connnection idle timeout (the wait_timeout
//       throw err;                                  // server variable configures this)
//     }
//   });
  
//   }
  
  
//   handleDisconnect();

/* GET home page. */
router.get('/', function(req, res) {
  res.writeHead(200, { 'content-type': 'text/html' })
  fs.createReadStream('./views/layouts/register.html').pipe(res)
});

router.get('/register', function(req, res) {
  res.writeHead(200, { 'content-type': 'text/html' })
  fs.createReadStream('./views/layouts/register.html').pipe(res)
});

router.get('/santaapi/getnames', santaController.getNames);
router.get('/santaapi/getregisternames', santaController.getRegisterNames);
router.post('/santaapi/makealotment', santaController.makeAlotment);
router.post('/santaapi/registersanta', santaController.registerDetails);

router.get('/people', function(req, res) {
  res.writeHead(200, { 'content-type': 'text/html' })
  fs.createReadStream('./views/layouts/people.html').pipe(res)
});


// router.get('/clubs/technical', clubsController.getTechnicalClubs);

// router.get('/clubs/cultural', clubsController.getCulturalClubs);

// router.get('/clubs/sports', clubsController.getSportsClubs);

// router.get('/clubs/sp', clubsController.getOtherClubs);

// router.get('/clubs/sb', clubsController.getStudentBodies);

// router.get('/clubs/searchall/search-clubs', clubsController.getSearchClubs);

// router.get('/clubs/getSearchIndex', clubsController.getClubsSearchIndex);

// router.get('/clubs/events', clubsController.getEvents);

// router.get('/clubs/:clubid', clubsController.getClub);

// router.get('/clubs/events/eventnames', clubsController.listEvents);

// router.get('/clubs/events/searchq', clubsController.searchEvents);

// router.get('/clubs/events/list', clubsController.listEventNames);

// router.get('/clubs/events/sortq', clubsController.sortEvents);

// router.get('/clubs/events/latest', eventsController.getLatestEvents);

// router.get('/clubs/events/:eventid', clubsController.getEvent);

// router.post('/clubs/events/register/:eventid',[
//   check('email').isEmail().withMessage('Email must be a proper email'),
//   check('name').isLength({ min: 5 }).withMessage('Name must be minimum 5 characters'),
//   check('college').isLength({ min: 2 }).withMessage('Please Enter proper college Name'),
//   check('cnum').isLength({min:9}).withMessage('Please enter a proper mobile number'),
//   check('reg').isLength({min:4}).withMessage('Please enter a proper registration number/college id'),
// ],captcha, clubsController.postRegisterEvent);

// router.get('/events/promotions/unsubscribe', eventsController.removeSubscriber);

// router.post('/events/promotions/subscribe', clubsController.postSubscriber);

// router.get('/clubs/faq/:clubid', clubsController.getFAQ);


// //Payment details

// router.get('/payment', eventsController.makePayments);
// router.get('/paytmCallback', eventsController.getPaytmCallback);
// router.post('/paytmCallback', eventsController.postPaytmCallback);
// router.post('/getTxnStatus', eventsController.getTransactionStatus);
// router.post('/updatePaymentStatus', eventsController.updatePaymentStatus);

module.exports = router;