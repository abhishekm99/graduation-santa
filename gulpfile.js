'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
 
sass.compiler = require('node-sass');
 
gulp.task('sass', function() {
  return gulp.src('./public/sass/**/*.scss') // Gets all files ending with .scss in app/scss and children dirs
    .pipe(sass())
    .pipe(gulp.dest('./public/stylesheets'))
})
 
gulp.task('watch', function(){
   gulp.watch('./public/sass/**/*.scss', gulp.series('sass')); 
  // Other watchers
})