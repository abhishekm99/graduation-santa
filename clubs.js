const Sequelize = require('sequelize');

const sequelize = require('../helpers/database');

const Club = sequelize.define('club', {
    id: {
        type:Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    clubname:{
        type:Sequelize.STRING,
        allowNull:false
    },
    clubdesc:{
        type:Sequelize.STRING,
        allowNull:false
    },
    clubcontact:{
        type:Sequelize.STRING,
        allowNull:false
    },
    clubemail:{
        type:Sequelize.STRING,
        allowNull:false
    },
    clublogourl:{
        type:Sequelize.STRING,
        allowNull:false
    },
},
{
    timestamps: false,
})

module.exports = Club;