var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var hbs = require('express-handlebars');
var Handlebars = require('handlebars');
var expressValidator = require('express-validator');
var bodyParser = require('body-parser');
var indexRouter = require('./routes/index');
// const authRouter = require('./routes/auth');
// const apiRouter = require('./routes/api');
const moment= require('moment')
var favicon = require('serve-favicon');
const cors = require('cors');
const flash = require('connect-flash');
// const User = require('./models/users');
const Names = require('./models/names');
const session = require('express-session');
const sequelize = require('./helpers/database');
var SequelizeStore = require('connect-session-sequelize')(session.Store);
const csrf = require('csurf');


var app = express();

var myStore = new SequelizeStore({
    db: sequelize
})

app.use(logger('dev'));
app.use(flash());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
// app.use(expressValidator())

app.use(session({
    name: 'SSID',
    secret: 'proxyspotting xml',
    unset: 'destroy',
    saveUninitialized: true,
    store: myStore,
    resave: false, // we support the touch method so per the express-session docs this should be set to false
    //proxy: true, // if you do SSL outside of node.
    checkExpirationInterval: 15 * 60 * 1000, // The interval at which to cleanup expired sessions in milliseconds.
    expiration: 24 * 60 * 60 * 1000  // The maximum age (in milliseconds) of a valid session.
}))

//myStore.sync();

app.use((req,res,next) => {
  if(!req.session.user){
      return next();
  }
  User.findByPk(req.session.user.clubid)
  .then(user => {
      req.user = user;
      next();
  }).catch(err => console.log(err));
});

// const csrfProtection = csrf();

// app.use(csrfProtection);

app.use(cors(corsOptions));


var corsOptions = {
  origin: 'https://www.manipalmarathon.in, http://localhost:3000',
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}




app.use((req,res,next) => {
  // res.locals.csrfToken = req.csrfToken();
  res.locals.isAuthenticated = req.session.isLoggedIn;
  next();
})

app.use('/', indexRouter);
// app.use('/admin', authRouter);
// app.use('/api', apiRouter);

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');
app.use(favicon(__dirname + '/public/images/SVG/ico.png'));

app.use(logger('dev'));
//app.use(cookieParser('proxyspotting xml'));
// app.use(expressValidator());
//app.use(expressSession({secret: 'max', saveUninitialized: false, resave: false}));
// view engine setup
app.engine('hbs', hbs({
  extname: 'hbs',
  defaultLayout: 'layout',
  layoutsDir: __dirname + '/views/layouts',
  partialsDir  : [
    //  path to your partials
    __dirname + '/views/partials',
  ]
}));

sequelize.sync().then().catch(err => console.log(err));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});



//Custom handlebar helper
Handlebars.registerHelper('ifCond', function(v1, v2, options) {
  if(v1 === v2) {
    return options.fn(this);
  }
  else if(String(v1)===String(v2)){
    return options.fn(this);
  }
  return options.inverse(this);
});

Handlebars.registerHelper('formatDate', function(dateString) {
  return new Handlebars.SafeString(
      moment(dateString).format("D MMM").toUpperCase()
  );
});

Handlebars.registerHelper('times', function(n, block) {
  var accum = '';
  for(var i = 1; i <= n; ++i)
      accum += block.fn(i);
  return accum;
});

Handlebars.registerHelper('split', function(plaintext) {
  var i, output = '',
        lines = plaintext.split(/\r\n|\r|\n/g);
    for (i = 0; i < lines.length; i++) {
        if(lines[i]) {
            output += '<p>' + lines[i] + '</p>';
        }
    }
  return new Handlebars.SafeString(output);
});

Handlebars.registerHelper('json', function (content) {
  return JSON.stringify(content);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json(err.message)
});



module.exports = app;