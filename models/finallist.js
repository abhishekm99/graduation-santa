const Sequelize = require('sequelize');
const sequelize = require('../helpers/database');
const { NULL } = require('node-sass');

const FinalList = sequelize.define('finallist', {
    id: {
        type:Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    primaryname:{
        type:Sequelize.STRING,
        allowNull:false
    },
    secondaryname:{
        type:Sequelize.STRING,
        allowNull:false
    }
},
{
    timestamps: false,
})

module.exports = FinalList;