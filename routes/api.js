var express = require('express');
var router = express.Router();

const eventsController = require('../controllers/events');
const clubsController = require('../controllers/clubs')

const dotenv = require('dotenv');


//API

//Announcements

router.get('/announcements', eventsController.getAnnouncements);
router.get('/colleges', eventsController.returnColleges);
router.get('/mitpostapi', eventsController.sendEventsToPost);
router.get('/club/getvideo', clubsController.getClubVideo);
router.post('/clubs/faq/askquestion', clubsController.askQuestion);


module.exports = router;